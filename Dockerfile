FROM i386/ubuntu:latest

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=master
ENV branch=$branch

RUN apt-get update -y && apt-get upgrade -y \
	&& apt-get install -y \
		build-essential \
		bash \
		bc \
		bzip2 \
		diffutils \
		g++ \
		gawk \
		gcc \
		git-core \
		gperf \
		gzip \
		libjson-perl \
		libncurses5-dev \
		lzop \
		make \
		patch \
		patchutils \
		perl \
		sed \
		tar \
		texinfo \
		unzip \
		wget \
		xfonts-utils \
		xsltproc \
		xz-utils \
		zip \
		libssl-dev \
		u-boot-tools \
		sudo \
		libglib2.0-dev \
		doxygen \
		bsdmainutils \
		swig \
		curl \
		libcurl4-gnutls-dev \
		libx11-dev \
		mesa-common-dev \
		libglu1-mesa-dev \
		libasound2 \
		xxd \
		meson \
		ninja-build \
		nasm \
		python3 \
		python3-pip \
		p7zip-full \
		libass-dev \
		libpcap-dev \
		imagemagick \
		optipng \
		rsync \
		libsdl1.2-dev \
		libsdl-image1.2-dev \
		libsdl-mixer1.2-dev \
		libsdl-net1.2-dev \
		libsdl2-dev \
		libsdl2-image-dev \
		libsdl2-mixer-dev \
		libsdl2-net-dev \
		libltdl-dev \
		libtool \
		pkg-config \
		libglew-dev \
		gnupg \
		ca-certificates \
	&& rm -rf /var/lib/apt/lists/*

RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list \
	&& apt-get update -y \
	&& apt-get build-dep -y retroarch \
	&& rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://apt.kitware.com/keys/kitware-archive-latest.asc | sudo apt-key add - \
 && echo  >> /etc/apt/sources.list \
 && echo "###### Ubuntu Update Repos" >> /etc/apt/sources.list \
 && echo "deb https://apt.kitware.com/ubuntu/ bionic main" >> /etc/apt/sources.list \
 && apt-get update -y \
 && apt-get install -y cmake \
 && rm -rf /var/lib/apt/lists/*

RUN useradd -d /developer -m developer && \
    chown -R developer:developer /developer && \
    echo "developer:developer" | chpasswd && \
    adduser developer sudo

ENV HOME=/developer

USER root
WORKDIR /developer
VOLUME /developer

CMD /usr/bin/linux32 /bin/bash
